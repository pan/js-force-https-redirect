# js-force-https-redirect

If the webpage is loaded using http, it is redirected using https.
This should be done on the server side but in some case it is not possible.

Use this only if you don't have access to the configuration of you web server!

# How to: nodejs

npm install js-force-https-redirect
then just add require('js-force-https-redirect') and that should work.

# How to: browserify

after cloning this repo:
browserify index.js > js-force-https-redirect.js

and then in you webpage:
<script src="js-force-https-redirect.js"></script>
